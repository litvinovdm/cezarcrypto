def rotate_text(text_data, num_rotate):
    new_str = ""
    # Round the rotate number to 26
    if num_rotate <= -26:
        num_rotate = (num_rotate % 26)
    if num_rotate < 0:
        num_rotate += 26
    if num_rotate >= 26:
        num_rotate = (num_rotate % 26)

    for i in range(0, len(text_data)):
        s = text_data[i]
        number = ord(s)
        # Upper-case (65 - 90)
        if 64 < ord(s) < 91:
            number = ord(s) + num_rotate
            if number > 90:
                number = 64 - (90 - ord(s)) + num_rotate

        # Lower-case (97 - 122)
        if 96 < ord(s) < 123:
            number = ord(s) + num_rotate
            if number > 122:
                number = 96 - (122 - ord(s)) + num_rotate

        s = chr(number)
        new_str += s
    return new_str
