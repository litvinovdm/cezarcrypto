$(function () {
    google.charts.load("current", {packages: ['corechart']});
    google.charts.setOnLoadCallback(drawChart);
});

function clearInputText() {
    $("#InputText").val("")
    makeFreqAnalis();
}

function clearResultText() {
    $("#ResultText").val("")
}

function copyResToOrig() {
    $("#InputText").val($("#ResultText").val())
    makeFreqAnalis();
}

function drawChart(Char, CharFreq) {
    var i = 1;
    var ChartData = [];
    ChartData[0] = ["Frequrency", "1", {role: "style"}];
    ChartData[1] = ["a", 0, "gold"];
    for (var key in CharFreq) {
        ChartData[i] = [Char[key], CharFreq[key], "gold"]
        i++;
    }
    var data = google.visualization.arrayToDataTable(ChartData);
    var view = new google.visualization.DataView(data);
    view.setColumns([0, 1,
        {
            calc: "stringify",
            sourceColumn: 1,
            type: "string",
            role: "annotation"
        },
        2]);

    var options = {
        title: "Frequency distribution of letters in original text",
        width: 1000,
        height: 300,
        bar: {groupWidth: "95%"},
        legend: {position: "none"},
    };
    var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
    chart.draw(view, options);
}

function makeFreqAnalis() {
    var SendData = {
        "Chars": [],
        "Char_Freq": [],
        "encrRot": 0,
        "text": $("#InputText").val(),
        "encrText": ""
    };
    $.post("/decrypt/", {
            data_json: JSON.stringify(SendData)
        },
        function (data) {
            //alert(data);
            var ReceivedData = data;
            var Char = [];
            var CharFreq = [];
            for (var ch in ReceivedData.Chars) {
                Char.push(ReceivedData.Chars[ch])
            }
            for (var ch in ReceivedData.Char_Freq) {
                CharFreq.push(ReceivedData.Char_Freq[ch])
            }
            if (ReceivedData.encrRot != "0") {
                $("#LabelInfo").text("The text is encrypted, Rotate to decrypt: " + ReceivedData.encrRot)
            }
            else {
                $("#LabelInfo").text("The text is not encrypted")
            }
            drawChart(Char, CharFreq);
        }
    );
}


function sampleText() {
    $.get("/samples/",
        function (data) {
            //alert(data);
            $("#InputText").val(data.text)
        }
    );
}

function encrypt() {
    var SendData = {
        "rotate_value": $("#RotValue").val(),
        "text": $("#InputText").val()
    };

    $.post("/encrypt/", {
            data_json: JSON.stringify(SendData)
        },
        function (data) {
            //alert(data);
            var ReceivedData = data;
            $("#ResultText").val(ReceivedData.text)
        }
    );
}

function decrypt() {
    var SendData = {
        "Chars": [],
        "Char_Freq": [],
        "encrRot": 0,
        "text": $("#InputText").val(),
        "encrText": ""
    };
    $.post("/decrypt/", {
            data_json: JSON.stringify(SendData)
        },
        function (data) {
            //alert(data);
            var ReceivedData = data;
            var str = "";
            var Char = [];
            var CharFreq = [];
            for (var ch in ReceivedData.Chars) {
                Char.push(ReceivedData.Chars[ch])
            }
            for (var ch in ReceivedData.Char_Freq) {
                CharFreq.push(ReceivedData.Char_Freq[ch])
            }
            $("#ResultText").val(ReceivedData.encrText)
            if (ReceivedData.encrRot != "0") {
                $("#LabelInfo").text("The text is encrypted, Rotate to decrypt: " + ReceivedData.encrRot)
            }
            else {
                $("#LabelInfo").text("The text is not encrypted")
            }
            drawChart(Char, CharFreq);

        }
    );
}

