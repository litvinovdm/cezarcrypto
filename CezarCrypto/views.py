from django.views.generic import View
from django.http import JsonResponse
from django.shortcuts import render
from lib.crypto import *
import samples
import json


# gives sample text
class SamplesView(View):
    def get(self, request):
        return JsonResponse(samples.textSample)

# take original text, rotate symbol and return him
class EncryptView(View):
    template_name = "main.html"

    def get(self, request):
        return render(request, "main.html")

    def post(self, request):
        # print request.POST
        if request.is_ajax():
            data = json.loads(request.POST['data_json'])
            data['text'] = rotate_text(data['text'], int(data['rotate_value']))
            return JsonResponse(data)
        return render(request, "main.html")

# take decrypted text, makes frequency analysis, found rotate value to decrypt, decrypted text and return it's all
class DecryptView(View):
    template_name = "main.html"

    def get(self, request):
        return render(request, "main.html")

    def post(self, request):
        # print request.POST
        if request.is_ajax():
            data = json.loads(request.POST['data_json'])
            char_freq = []
            decr_text = str(data['text']).lower()
            chars = []
            litters = []
            encr_rot = 0
            max_et = 0

            # Count litters frequrency
            for ch in decr_text:
                chars.append(ch)
            for litter_num in range(97, 123):
                litter = chr(litter_num)
                litters.append(litter)
                litter_freq = chars.count(litter)
                char_freq.append(litter_freq)

            # Found rotate with max litters e + t
            for rotate_num in range(0, 26):
                text = rotate_text(decr_text, rotate_num)
                sum_et = text.count("e") + text.count("t")
                if sum_et > max_et:
                    max_et = sum_et
                    encr_rot = rotate_num
                    # print str(rotate_num) + "  " + str(sumET) + "  " + str(maxET) + "  " + str(encrRot) + "\r\n"
            data['Chars'] = litters
            data['Char_Freq'] = char_freq
            data['encrRot'] = encr_rot
            data['encrText'] = rotate_text(data['text'], encr_rot)
            return JsonResponse(data)

        return render(request, "main.html")

# gives main page
class RootView(View):
    template_name = "main.html"

    def get(self, request):
        return render(request, "main.html")
